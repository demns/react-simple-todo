import React, { Component } from 'react';
import './App.css';
import List from './List.js'

class App extends Component {
  render() {
    return (
      <div className="App">
        <List 
          items={['banana', 'apple', 'tomato', 'cabbage']}
        />
      </div>
    );
  }
}

export default App;
