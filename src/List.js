import React, { Component } from 'react';
import Filter from './Filter.js';

export default class List extends Component {

    constructor(props) {
        super(props);
        this.state = {
            items: this.props.items,
        }

        this.onFilterChange = this.onFilterChange.bind(this);
    }

    onFilterChange(event) {
        let updatedList = this.props.items;

        updatedList = updatedList.filter((item) => {
            return item.toLowerCase().search(
                event.data.toLowerCase()) !== -1;
        });

        this.setState({
            items: updatedList
        });
    }

    render() {
        const { items } = this.state;

        return [
            <Filter key="filter"
                onFilterChange={this.onFilterChange}
            />,
            <ul key="list">
                {items.map((item, index) => (
                    <li key={index}>{item}</li>
                ))}
            </ul>
        ];
    }
}
