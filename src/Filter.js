import React, { Component } from 'react';

export default class Filter extends Component {
    constructor(props) {
        super(props);
        this.state = {value: ''};

        this.handleChange = this.handleChange.bind(this);
    }
    
    handleChange(event) {
        this.setState({value: event.target.value});
        this.props.onFilterChange({
            data: event.target.value,
            event: 'filterChange',
        });
    }
  
    render() {
        return (
            <input 
                onChange={this.handleChange}
                placeholder="Search"
                type="text"
                value={this.state.value}
            />
        );
    }
}
